# What is this?

This is my repository that holds common things I use in
[GitLab](https://gitlab.com/warsaw).  For now, it just includes my common
[.gitlab-ci.yml](https://gitlab.com/warsaw/gitlab-ci/-/raw/main/common-gitlab-ci.yml)
file.

For each repository you want to use this with, put the following in that
repo's `.gitlab-ci.yml` file:

```
include:
    - remote: https://gitlab.com/warsaw/gitlab-ci/-/raw/main/common-gitlab-ci.yml

variables:
    MODULE_PATH: "src/moduledir"
    MODULE_NAME: "mymodule"
```

All you need to do is set `MODULE_PATH` to the file system path of the source,
and `MODULE_NAME` to the appropriate Python import name and you're done.
