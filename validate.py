import sys
from importlib import import_module
from requests import get

id = sys.argv[1]
modname = sys.argv[2]
print(f'Validating: {modname}')

url = f'https://gitlab.com/api/v4/projects/{id}/repository/tags'
print(f'Getting tags from {url}')

r = get(url).json()
# Ordered desc by default.
top_tag = r[0]['name']
print(f'Got latest tag: {top_tag}')

mod = import_module(modname)
print(f'git tag {top_tag} == version {mod.__version__}')
assert top_tag == mod.__version__
